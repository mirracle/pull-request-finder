from urllib.request import urlopen, Request
from urllib.error import HTTPError
from base64 import b64encode
from json import loads
from webbrowser import open_new
from getpass import getpass

def get_open(username, password, all_pulls_urls):
	for items in all_pulls_urls:
		url = items+'?fields=values.links.html&q=state+%3D+%22OPEN%22'
		credentials = b64encode("{0}:{1}".format(username, password).encode()).decode("ascii")
		headers = {'Authorization': "Basic " + credentials}
		request = Request(url=url, headers=headers)
		connection = urlopen(request)
		content = connection.read().decode('utf-8')
		content_json = loads(content)
		result = [items['links']['html']['href'] for items in content_json['values']]
		for item in result:
			open_new(item)

def get_review(username, password, all_pulls_urls):
	for items in all_pulls_urls:
		url = items+'?fields=values.links.html&q=reviewers.username+%3D+%22{0}%22'.format(username)
		credentials = b64encode("{0}:{1}".format(username, password).encode()).decode("ascii")
		headers = {'Authorization': "Basic " + credentials}
		request = Request(url=url, headers=headers)
		connection = urlopen(request)
		content = connection.read().decode('utf-8')
		content_json = loads(content)
		result = [items['links']['html']['href'] for items in content_json['values']]
		for item in result:
			open_new(item)

def get_closed(username, password, all_pulls_urls):
	for items in all_pulls_urls:
		url = items+'?fields=values.links.html&q=closed_by.username+%3D+%22{0}%22'.format(username)
		credentials = b64encode("{0}:{1}".format(username, password).encode()).decode("ascii")
		headers = {'Authorization': "Basic " + credentials}
		request = Request(url=url, headers=headers)
		connection = urlopen(request)
		content = connection.read().decode('utf-8')
		content_json = loads(content)
		result = [items['links']['html']['href'] for items in content_json['values']]
		for item in result:
			open_new(item)

def pull_type(choice, username, password, all_pulls_urls):
	if choice == '1':
		get_open(username, password, all_pulls_urls)
	if choice == '2':
		get_review(username, password, all_pulls_urls)
	if choice == '3':
		get_closed(username, password, all_pulls_urls)

while True:
    try:
        print('-' * 88)
        username = input('Please enter your username or email on Bitbucket: ')
        password = getpass('Please enter your password: ')
        url = "https://api.bitbucket.org/2.0/repositories?fields=values.links.pullrequests&role=member"
        credentials = b64encode("{0}:{1}".format(username, password).encode()).decode("ascii")
        headers = {'Authorization': "Basic " + credentials}
        request = Request(url=url, headers=headers)
        connection = urlopen(request)
        content = connection.read().decode('utf-8')
        content_json = loads(content)
        all_pulls_urls = [items['links']['pullrequests']['href'] for items in content_json['values']]
        break
    except HTTPError as e:
        if e.code == 401:
            print('-' * 88)
            print(e)
            print('You entered the wrong password or username!')
            print('Please try again')
            print('Or use ctrl+C to exit')

while True:
	print('-' * 88)
	print('Searche for pull requests that has:')
	print('1. - an “open” status')
	print('2. - requesting a review from the user')
	print('3. - user has not approved or declined it')
	choice = input('Please enter your choice from above (1, 2 or 3): ')
	int_choice = int(choice)
	if int_choice >= 1 and int_choice <= 3:
		pull_type(choice, username, password, all_pulls_urls)
		print('-' * 88)
		print('Pull requests that you searche are openned in browser')
		print('You can repeat the search')
		print('Or use ctrl+C to exit')
	else:		
		print('-' * 88)
		print('You entered an incorrect value')
		print('Please try again')
		print('Or use ctrl+C to exit')

